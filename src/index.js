function highlightMenuItem(x) {
    if (x.matches) { // If media query matches
        let menu = document.querySelector('.menu');
        menu.addEventListener('click',function (event) {
            let target = event.target;
            let elderSelect = document.querySelector('.selectItem');
            if(elderSelect){
                elderSelect.classList.remove('selectItem');
            }
            target.classList.add('selectItem');
        });
    }
}

let xMedia = window.matchMedia("(max-width: 480px)");
highlightMenuItem(xMedia);
xMedia.addEventListener("change",highlightMenuItem);


import './scss/reset.scss'
import './css/font.css'
import './scss/index.scss'
